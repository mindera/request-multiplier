'use strict';

const nock = require('nock');
const http = require('http');
const request = require('request-promise');
const expect = require('chai').expect;
const index = require('../');

describe('index', () => {
  let serverInstance;
  const opts = {
    uri: 'http://my.little.nock.server.com',
    port: 7777,
    multiplier: 3,
    methods: ['GET', 'HEAD'],
    uriHeader: 'x-target-url'
  };


  beforeEach(() => {
    if (serverInstance) {
      serverInstance.close();
    }

    serverInstance = index(opts);
  });

  afterEach(() => nock.cleanAll());

  it('should be an HTTP server', () => {
    expect(serverInstance).to.be.an.instanceOf(http.Server);
  });

  it('should perform requests to target uri', (done) => {
    const scope = nock(opts.uri)
      .get('/success')
      .times(opts.multiplier)
      .reply('200');

    request({
      url: 'http://localhost:' + opts.port,
      method: 'GET',
      headers: {
        'x-target-url': 'http://localhost:' + opts.port + '/success',
      },
    })
    .catch(done);

    // kinda sucks this method
    setTimeout(() => {
      scope.done();
      done();
    }, 1000);
  });

  it('should ignore errors', (done) => {
    const scope = nock(opts.uri)
      .get('/error')
      .times(opts.multiplier)
      .reply('503');

    request({
      url: 'http://localhost:' + opts.port,
      method: 'GET',
      headers: {
        'x-target-url': 'http://localhost:' + opts.port + '/error',
      },
    })
    .catch(done);

    // kinda sucks this method
    setTimeout(() => {
      scope.done();
      done();
    }, 1000);
  });
});
