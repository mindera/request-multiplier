'use strict';

const http = require('http');
const expect = require('chai').expect;
const sinon = require('sinon');
const request = require('supertest');
const server = require('../src/server.js');

describe('Server', () => {
  let serverInstance;

  beforeEach(() => {
    serverInstance = server({ methods: ['GET', 'HEAD'] });
  });

  it('should be an HTTP server', () => {
    expect(serverInstance).to.be.an.instanceOf(http.Server);
  });

  it('should accept any request', (done) => {
    request(serverInstance)
      .get('/')
      .expect(200, '', done);
  });

  it('should trigger a requestReceived event', (done) => {
    const spy = sinon.spy(() => done());

    serverInstance.on('requestReceived', spy);

    request(serverInstance)
      .get('/')
      .expect(200, '')
      .end(() => {
        expect(spy.called).to.equal(false);
      });
  });

  it('should passed the received request to the event listener', (done) => {
    const spy = sinon.spy((req) => {
      expect(req.url).to.equal('/');
      expect(req.method).to.equal('GET');
      expect(req.header).to.be.an.object;
      done();
    });

    serverInstance.on('requestReceived', spy);

    request(serverInstance)
      .get('/')
      .end();
  });

  it('should not allow requests with forbidden methods', (done) => {
    request(serverInstance)
      .post('/')
      .expect(503)
      .end(done);
  });
});
