const multiplier = require('../src/multiplier');
const expect = require('chai').expect;

describe('Multiplier', () => {

  it('should return a Number', () => {
    const result = multiplier('0.5');
    expect(result).to.be.a('number');
  });

  it('should return an integer', () => {
    const result = multiplier('0.5');
    expect(parseInt(result, 10)).to.equal(result);
  });

  it('should return either 1 or 0', () => {
    const result = multiplier('0.5');
    expect(result).to.be.within(0, 1);
  });

  it('should return 1 or 2', () => {
    const result = multiplier('1.5');
    expect(result).to.be.within(1, 2);
  });

  it('should return 7 or 8', () => {
    const result = multiplier('7.35');
    expect(result).to.be.within(7, 8);
  });

  it('should return exactly 2', () => {
    const result = multiplier('2');
    expect(result).to.equal(2);
  });

  it('should return exactly 1024', () => {
    const result = multiplier(1024);
    expect(result).to.be.equal(1024);
  });

  it('should have result in an interval of ± 5', () => {
    const multi = (x) => () => multiplier(x);
    const sample = Array.apply(null, { length: 100 }).map(multi(0.25));
    const ones = sample.filter((x) => x === 1);

    expect(ones.length).to.be.closeTo(25, 5)
  });
});
