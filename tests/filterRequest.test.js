const expect = require('chai').expect;
const filterRequest = require('../src/filterRequest');

describe('filterRequest', () => {
  describe('byMethod', () => {
    it('should return true if method is GET', () => {
      const allowGet = filterRequest.byMethod(['GET']);
      expect(allowGet({ method: 'GET' })).to.equal(true);
    });

    it('should return true if method is GET or HEAD', () => {
      const allowGetAndHead = filterRequest.byMethod(['GET', 'HEAD']);

      expect(allowGetAndHead({ method: 'GET' })).to.equal(true);
      expect(allowGetAndHead({ method: 'HEAD' })).to.equal(true);
    });

    it('should return false if method is not GET or HEAD', () => {
      const allowGetAndHead = filterRequest.byMethod(['GET', 'HEAD']);

      expect(allowGetAndHead({ method: 'POST' })).to.equal(false);
      expect(allowGetAndHead({ method: 'PUT' })).to.equal(false);
    });
  });
});
