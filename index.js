const R = require("ramda");
const server = require("./src/server");
const configuration = require("./config");

// init :: Object -> http.Server
const init = (config) => {
  console.log("Started application with config: ", config);
  return server(config).listen(config.get("port"));
};

module.exports = R.compose(init, configuration.merge.bind(configuration));
