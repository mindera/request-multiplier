/**
 * Convert a multiplier number into an integer
 * - A multiplier like 0.75 means that 1 in every 4 times (estimate) the resulting
 * number will be 0.
 * - A multiplier like 7.35 means that approximatedly 1 in every 3 times,
 * the resulting number will be 8
 */

// multiplier :: (String) -> Number
module.exports = (val) => {
  const float = parseFloat(val || 1);
  const integer = Math.floor(float);
  const fraction = float % (integer || 1);

  return (Math.random() <= fraction) ?
    integer + 1 :
    integer;
};
