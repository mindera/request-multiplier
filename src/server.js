const restify = require('restify');
const middleware = require('./middleware');

// success :: (http.IncomingMessage, http.ServerResponse, Function) -> a
const success = (request, response, next) => {
  response.send(200);
  return next();
};

module.exports = (config) => {
  const server = restify.createServer();

  server.use(middleware(config));

  server.get(/.*/, success);
  server.post(/.*/, success);
  server.put(/.*/, success);
  server.del(/.*/, success);
  server.head(/.*/, success);

  return server;
};
