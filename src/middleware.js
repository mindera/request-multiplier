const R = require('ramda');
const url = require('url');
const multiplier = require('./multiplier');
const client = require('./client');

// middleware :: (Object) -> (http.IncomingMessage, http.ServerResponse, Function) -> Promise
module.exports = (config) => {
  const buildHeaders = (req) => {
    const headers = Object.assign({}, req.headers);
    headers[config.signature] = config.get('user-agent');
    return headers;
  };

  const getProtocol = (req) => R.has(config.get('headers.secure'), req.headers) ? 'https:' : 'http:';
  const getHeaders = R.prop('headers');
  const getHost = R.compose(R.prop(config.get('headers.url')), getHeaders);
  const isSelfMadeRequest = R.compose(
    R.equals(config.get('user-agent')),
    R.prop(config.get('headers.signature')),
    getHeaders
  );
  const isAllowedMethod = (req) => R.contains(R.toLower(req.method), config.get('methods'));

  console.log('Loaded middledware');
  return (request, response, next) => {
    console.log('Received request', request.url);

    const stats = {
      success: 0,
      error: 0,
      done: 0,
    };

    if (isSelfMadeRequest(request)) {
      response.send(200);
      return next();
    }

    if (!isAllowedMethod(request)) {
      response.send(200);
      return next();
    }

    console.log('Processing request', url.format({
      protocol: getProtocol(request),
      host: getHost(request),
      pathname: request.url,
    }));

    const requests = Array.from({ length: multiplier(config.get('multiplier')) })
      .map(() => client({
        uri: url.format({
          protocol: getProtocol(request),
          host: getHost(request),
          pathname: request.url,
        }),
        method: request.method,
        headers: buildHeaders(request),
        body: request.body,
      })
      .then(() => (stats.success += 1))
      .catch(() => (stats.error += 1))
    );

    stats.done = requests.length;

    return Promise.all(requests)
      .then(() => console.log(stats)) // eslint-disable-line no-console
      .then(() => next())
      .catch(next);
  };
};
