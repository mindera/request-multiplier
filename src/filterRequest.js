const R = require('ramda');

// I'm so over-engineering this stuff...
// byMethod :: ([String], http.IncomingMessage) -> Boolean
exports.byMethod = R.memoize(R.curry((methods, request) => R.contains(R.toUpper(request.method), methods.map(R.toUpper))));

exports.getHeaders = R.prop('headers');
