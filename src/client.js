const request = require('request-promise');

request.defaults = {
  forever: true,
  resolveWithFullResponse: true,
};

module.exports = request;
