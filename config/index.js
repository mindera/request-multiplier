'use strict';

const R = require('ramda');
const deepAssign = require('deep-assign');
const configFile = require('./config.json');


// Container :: (a) -> Container a
class Container {

  static of(val) {
    return new Container(val);
  }

  constructor(val) {
    this._value = val;
  }

  // map :: (a -> b) -> Container a -> Container b
  map(f) {
    return Container.of(f(this._value));
  }

  // get :: (String) -> a
  get(path) {
    return R.path(R.split('.', path), this._value);
  }

  // get :: (Object) -> Container a
  merge(val) {
    return Container.of(deepAssign({}, this._value, val));
  }
}

module.exports = Container.of(configFile);
