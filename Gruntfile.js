// I'm only using Grunt to package the application into a RPM
module.exports = (grunt) => {
  grunt.loadNpmTasks('grunt-easy-rpm');

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    buildNumber: process.env.BUILD_NUMBER || 1,
    easy_rpm: {
      options: {
        name: 'request-multiplier',
        version: '<%= pkg.version %>',
        release: '<%= buildNumber %>',
        buildArch: 'noarch',
        requires: ['node >= 4.2.4'],
        license: 'Mindera All Rights Reserved',
        vendor: 'Mindera',
        group: 'Applications',
        url: 'git@github.com:Mindera/<%= pkg.name %>.git',
        autoReq: false,
        autoProv: false,
      },
      release: {
        files: [
          { src: 'src/**/*', dest: '/opt/<%= pkg.name %>' },
          { src: 'conf/**/*', dest: '/opt/<%= pkg.name %>' },
          { src: '.js', dest: '/opt/<%= pkg.name %>' },
          {
            src: '**',
            dest: '/opt/<%= pkg.name %>/node_modules',
            cwd: 'node_modules',
          },
        ],
      },
    },
  });

  grunt.registerTask('default', 'easy_rpm');
};
