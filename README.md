# Request Multiplier

## Options

```
  Usage: request-multipler [options]

  Options:

  -h, --help                 output usage information
  -V, --version              output the version number
  -c, --config <filepath>    Load configuration from file (is merged with defaults and takes precedence from CLI options)
  -p, --port <n>             Listen on specified port. Defaults: 9001
  -x, --multiplier <n>       If smaller than 1, then the given percentage of requests will be dropped
  -m, --methods <methods>    Defaults: GET, HEAD, OPTIONS, TRACE
  -u, --uri-header <header>  Specify from which header it should fetch the target URL. Defaults: Host
  -a, --user-agent <header> Specify the user agent for the request (this is also used to avoid infinite loops)
```

## Commands

  * `npm run test` - Run application tests.
  * `npm run package` - Creates a RPM package.
  * `npm run release` - Runs tests and create the RPM package.
  * `npm run clean` - Remove generated RPM.

## Examples

```
request-multipler -c /etc/request_multiplier.conf
```
Read the JSON configs from the file. These configurations are merged with the
application's defaults.
*Note:* The application won't break if the config file doesn't exist. It simply
uses the default options.

```
request-multipler -p 8080 -u host -m GET,HEAD -x 0.75
```

The above command, will set up a server listening on port 8080, and for every
request that it will receive, it will check if the header `host` is present
and makes at least one request to the uri in the x-proxy-pass value.
with 75% chances of making a second request to the target url.
It will make requests if the incoming request has a GET or HEAD method.

```
request-multipler -p 8080 -x 1000 -m GET,HEAD
```

The above command, will set up a server listening on port 8080, and for every
request that it will receive, it will make exactly 1024 requests to target uri
given in the `host` header from the incoming request.
It will make requests if the incoming request has a GET or HEAD method.


# TODO

- [ ] Organize configuration loading
- [ ] Allow multiple target URI
- [ ] Improve multiplier tests to conform with a [Confidence Interval](https://en.wikipedia.org/wiki/Confidence_interval)
- [ ] Improve the display of requests' status (Success/Failures)
- [ ] Improve logging
